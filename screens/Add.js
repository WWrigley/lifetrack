import React from 'react';
import { StyleSheet, View, DatePickerAndroid, AsyncStorage } from 'react-native';
import {  Button, FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import moment from 'moment';

const ERRORTEXT = 'This field is required'

class AddScreen extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            goalTitle : "",
            goalDescription: "",
            goalRepeat : "",
            goalEndDate : moment().format('Do MMM YY'),
            error: ''
        }
        this.onDatePickerClick = this.onDatePickerClick.bind(this);
        this.validateTextField = this.validateTextField.bind(this);
        this.onFormSubmitClick = this.onFormSubmitClick.bind(this);
    }

    validateTextField(text){
        if(!text){
            this.setState({error: ERRORTEXT})
        } else {
            this.setState({error: ""})
        }
    }

    onFormSubmitClick(){
        //TODO
    }

    onDatePickerClick(){
        try {
            DatePickerAndroid.open({
                date: new Date(),
                mode: 'calendar'
            }).then((data) => {
                if(data.action !== DatePickerAndroid.dismissedAction ){
                  let enteredDate = moment({year: data.year, day: data.day, month: data.month})
                  this.setState({goalEndDate: enteredDate.format('Do MMM YY')})
                }
            })
        } catch({code, message}) {
            console.warn('Cannot open date picker', message);
        }
       

    }

    render(){
        return(
            <View style={{flex: 1, alignItems: 'flex-start', justifyContent: 'flex-start'}}>
               <FormLabel labelStyle={{fontSize: 25, color: 'black'}}> Create New Goal </FormLabel>
               <FormLabel labelStyle={styles.Text}> Goal Title: </FormLabel>
               <FormInput returnKeyType= 'done' style={styles.textInput} onEndEditing = {() => this.validateTextField(this.state.goalTitle)} onChange = {(text) => {
                        this.setState({goalTitle: text })
                    }}/>
                <FormValidationMessage>{this.state.error}</FormValidationMessage>
                <FormLabel labelStyle={styles.Text}> Goal Description: </FormLabel>
                <FormInput returnKeyType= 'done' style={styles.textInput} onChange = {(text) => {
                        this.setState({goalDescription: text })
                    }}/>
                <FormLabel labelStyle={styles.Text}> Goal End Date: </FormLabel>
                <View flexDirection= 'row'>
                    <FormLabel labelStyle={styles.Text}> {this.state.goalEndDate} </FormLabel>
                    <Button style={styles.Button} backgroundColor= '#24c41f' title= 'Choose' rounded= {true}  onPress = {()=> this.onDatePickerClick}/>
                </View>
                <FormLabel labelStyle={styles.Text}> I will work towards this goal: </FormLabel>
                <View flexDirection= 'row'>
                    <FormInput returnKeyType = 'done' keyboardType='numeric' containerStyle={{width: 40}} onEndEditing = {() => this.validateTextField(this.state.goalTitle)} onChange= {(text) => {
                        this.setState({goalRepeat : text})
                    }}/>
                    <FormLabel labelStyle={styles.Text}> times a week </FormLabel>
                </View>
                <FormValidationMessage>{this.state.error}</FormValidationMessage>
                <Button style={{fontSize: 20}} backgroundColor= '#24c41f' title= 'Submit' rounded= {true}  onPress = {()=> this.formSubmitClick}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textInput : {
        marginLeft: 20,
        width: 200,
        paddingTop: 15,
        padding: 5
    },

    Text : {
        fontSize: 15,
        color: 'black'
    },

    Button : {
        marginTop: 10
    }
})

export default AddScreen;
