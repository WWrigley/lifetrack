import React from 'react';
import { StyleSheet, Text, View, Button, TouchableWithoutFeedback, AsyncStorage } from 'react-native';

const styles = StyleSheet.create({
  addButton : {
    fontSize: 30,
    color: '#fff',
    paddingRight: 20,
  }
})

class HomeScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      dataSource : []
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerRight : (
        <TouchableWithoutFeedback onPress = {() => {
          navigation.navigate('Add');
        }}>
        <View>
          <Text style={styles.addButton}>+</Text>
        </View>
      </TouchableWithoutFeedback>
      ),
    }
  }

  componentDidMount(){
    AsyncStorage.getAllKeys((err, keys) => {
      AsyncStorage.multiGet(keys, (err, stores) => {
        console.warn(stores);
      })
    })
  }

    render() {
      return (
        <View style={{ flex: 1}}>
        </View>
      );
    }
  }

export default HomeScreen;