import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import HomeScreen from './screens/Home';
import DetailsScreen from './screens/Details';
import AddScreen from './screens/Add';

const RootStack = createStackNavigator(
  {
  Home : {
    screen: HomeScreen
  },
  Details : {
    screen: DetailsScreen
  },
  Add : {
    screen : AddScreen
  }
},
{
  initialRouteName: 'Home',
  navigationOptions : {
    title: 'LifeTrack',
    headerStyle: {
      backgroundColor: '#24c41f',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }

})

export default class App extends React.Component {
  render() {
    return (
      <RootStack/>
    );
  }
}